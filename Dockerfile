FROM nginx
LABEL MAINTAINER="Paulo Siqueira <pmp.paulosiqueira@gmail.com>"
LABEL SITE_VERSION="1.0.0"
COPY cloudedemais /usr/share/nginx/html
RUN apt-get update && apt-get install -y vim nano
WORKDIR /usr/share/nginx/html